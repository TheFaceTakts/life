#include <unistd.h>
#include <stdio.h>

int findColon(char *str) {
    int colonPos = 0;
    while (str[colonPos] != ':') {
        ++colonPos;
    }
    str[colonPos] = '\0';
    return colonPos;
}


void writeSomething(int fd, const void *buffer, int size) {
    int writen = 0;
    int curWriten = 0;
    do {
        curWriten = write(fd, buffer, size - writen);
        writen += curWriten;
        // printf("%d %d\n", size, writen);
    } while (writen != size);
}


void readSomething(int fd, void *buffer, int size) {
    int alreadyRead = 0;
    int curRead = 0;
    do {
        // printf("%d %d %d\n", size, curRead, alreadyRead);
        curRead = read(fd, buffer, size - alreadyRead);
        alreadyRead += curRead;
        // printf("%d %d %d\n", size, curRead, alreadyRead);
    } while (alreadyRead != size);
}


void writeToSocket(int sd, const void *buffer, int size) {
    writeSomething(sd, &size, sizeof(int));
    writeSomething(sd, buffer, size);
}

void readFromSocket(int sd, void *buffer, int &size) {
    readSomething(sd, &size, sizeof(int));
    readSomething(sd, buffer, size);
}
