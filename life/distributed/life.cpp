#include "node.h"


int main(int argc, char **argv) {
    Node node;
    if (argc > 3) {
        node.initHost(atoi(argv[1]), argv[2], argv[3], atoi(argv[4]));
        node.startCalculation();
        node.getFieldsHost();
        return 0;
    } else {
        node.initRegularNode(argv[1], argv[2]);
        node.startCalculation();
        node.sendField();
    }
    return 0;
}
