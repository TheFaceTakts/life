#ifndef NODE_H
#define NODE_H
#include "utils.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sched.h>

struct Node {
    struct ConnectItem {
        int sd;
        struct sockaddr_in addr;
        int sock[2];
        bool sock_inited;

        ConnectItem() {
            sock_inited = false;
        }

        ConnectItem(char *host) {
            printf("CI %s\n", host);
            int colonPos = findColon(host);
            sd = socket(AF_INET, SOCK_STREAM, 0); //possible error
            if (sd == -1) {
                perror("socket");
            }
            addr.sin_family = AF_INET;
            addr.sin_port = htons(atoi(host + colonPos + 1));
            addr.sin_addr.s_addr = inet_addr(host);
            host[colonPos] = ':';
        }

        void connectSocket() {
            while (connect(sd, (struct sockaddr *)&addr, sizeof(addr))) {
                sleep(5);
                perror("Connect");
                sched_yield();
            }
        }

        void initSockets() {
            for (int i = 0; i < 2; ++i) {
                sock[i] = accept(sd, NULL, NULL);
            }
            sock_inited = true;
        }

        void setupServer(int listenCount) {
            const int optVal = 1;
            if (setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, (void*) &optVal,
                           sizeof(optVal))) {
                perror("setsockopt");
            }
            if (bind(sd, (struct sockaddr *)&addr, sizeof(addr))) {
                perror("bind");
            }
            if (listen(sd, listenCount)) {
                perror("listen");
            }
        }

        void receiveData(char *buffer[2], int size, int right) {
            for (int i = 0; i < 2; ++i) {
                // printf("SOCK %d\n", sock[i]);
                int realSender;
                readSomething(sock[i], &realSender, sizeof(int));
                readSomething(sock[i], buffer[realSender == right], size);
            }
        }

        ~ConnectItem() {
            close(sd);
            if (sock_inited) {
                for (int i = 0; i < 2; ++i) {
                    close(sock[i]);
                }
            }
        }
    };

    struct Move {
        int dx;
        int dy;
    };

    static const int MOVE_CNT = 8;
    Move moves[MOVE_CNT];

    ConnectItem *neighbours[2];
    ConnectItem *server;
    ConnectItem *me;
    int *othersSocketList;
    char **field[2];
    char *borders[2];

    static const char STATE[2];

    int N, M;
    int steps;
    int index;
    int total;
    int totalFieldN;
    int curField;

    void readFieldHost(FILE *file, char *bufferline) {
        field[0] = new char* [N];
        field[1] = new char* [N];
        for (int i = 0; i < N; ++i) {
            field[0][i] = new char[M];
            field[1][i] = new char[M];
            fscanf(file, "%s", bufferline);
            memcpy(field[0][i], bufferline, M * sizeof(char));
        }
    }

    void sendField(FILE *file, char *bufferline, int n, int sd) {
        writeSomething(sd, &n, sizeof(int));
        writeSomething(sd, &M, sizeof(int));
        for (int i = 0; i < n; ++i) {
            fscanf(file, "%s", bufferline);
            writeSomething(sd, bufferline, M);
        }
    }

    void initHost(int unitsCount, char *self, char *filename, int steps) {
        this->steps = steps;
        this->total = unitsCount;
        this->index = 0;
        othersSocketList = new int[unitsCount];
        setupMyServer(self, unitsCount + 1);
        server = nullptr;
        FILE *inputFile = fopen(filename, "r");

        int n, m;
        fscanf(inputFile, "%d %d", &n, &m);

        totalFieldN = n;
        N = (n / unitsCount) + (0 < n % unitsCount);;
        M = m;

        char *bufferline = new char[m + 1];

        readFieldHost(inputFile, bufferline);

        char *prevAddress = new char[strlen(self) + 1];
        strcpy(prevAddress, self);
        int prevSocket = 0;

        for (int i = 1; i < unitsCount; ++i) {
            int otherSd = accept(me->sd, (struct sockaddr *)(NULL), NULL);
            othersSocketList[i] = otherSd;
            if (otherSd == -1) {
                perror("accept");
                return;
            }
            printf("%d\n", otherSd);

            writeSomething(otherSd, &steps, sizeof(int));
            writeSomething(otherSd, &unitsCount, sizeof(int));
            writeSomething(otherSd, &i, sizeof(int));

            sendField(inputFile, bufferline,
                      (n / unitsCount) + (i < n % unitsCount),
                      otherSd);
            writeToSocket(otherSd, prevAddress, strlen(prevAddress) + 1);
            int size;
            readSomething(otherSd, &size, sizeof(int));
            delete[] prevAddress;
            prevAddress = new char[size + 1];
            readSomething(otherSd, prevAddress, size);
            if (i == 1) {
                neighbours[1] = new ConnectItem(prevAddress);
            } else {
                writeToSocket(prevSocket, prevAddress, size + 1);
            }
            prevSocket = otherSd;
        }
        writeToSocket(prevSocket, self, strlen(self));
        neighbours[0] = new ConnectItem(prevAddress);

        delete[] prevAddress;
        delete[] bufferline;
        fclose(inputFile);
    }

    void initRegularNode(char *host, char *self) {
        printf("%s %s\n", host, self);
        setupMyServer(self, 3);
        connectToServer(host, self);
        readInts();
        readField();
        readNeighbours();
        othersSocketList = nullptr;
    }

    void connectToNeighbours() {
        for (int i = 0; i < 2; ++i) {
            neighbours[i]->connectSocket();
        }
    }

    void closeNeighbours() {
        for (int i = 0; i < 2; ++i) {
            close(neighbours[i]->sd);
        }
    }

    void setupMyServer(char *self, int listenCount) {
        me = new ConnectItem(self);
        me->setupServer(listenCount);
    }

    void readInts() {
        readSomething(server->sd, &steps, sizeof(int));
        readSomething(server->sd, &total, sizeof(int));
        readSomething(server->sd, &index, sizeof(int));
    }

    void readField() {
        readSomething(server->sd, &N, sizeof(int));
        readSomething(server->sd, &M, sizeof(int));
        field[0] = new char* [N];
        field[1] = new char* [N];
        for (int i = 0; i < N; ++i) {
            field[0][i] = new char[M];
            field[1][i] = new char[M];
            readSomething(server->sd, field[0][i], M);
        }
    }

    void sendField() {
        for (int i = 0; i < N; ++i) {
            writeSomething(server->sd, field[1 ^ curField][i], M);
        }
        write(server->sd, &N, 1);
    }

    void getFieldsHost() {
        char *bufferline = new char[M + 1];
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < M; ++j) {
                printf("%c", field[1 ^ curField][i][j]);
            }
            printf("\n");
        }
        for (int i = 1; i < total; ++i) {
            int curN = (totalFieldN / total) + (i < totalFieldN % total);
            for (int j = 0; j < curN; ++j) {
                readSomething(othersSocketList[i], bufferline, M);
                bufferline[M] = '\0';
                printf("%s\n", bufferline);
            }
        }
    }

    void readNeighbours() {
        for (int i = 0; i < 2; ++i) {
            int size;
            readSomething(server->sd, &size, sizeof(int));
            char *hostname = new char[size];
            readSomething(server->sd, hostname, size);
            neighbours[i] = new ConnectItem(hostname);
            delete[] hostname;
        }
    }

    void connectToServer(char *host, char *self) {
        server = new ConnectItem(host);
        server->connectSocket();
        writeToSocket(server->sd, self, strlen(self) + 1);
    }

    void initMoves() {
        int cnt = 0;
        for (int dx = -1; dx <= 1; ++dx) {
            for (int dy = -1; dy <= 1; ++dy) {
                if (dx || dy) {
                    moves[cnt++] = {dx, dy};
                }
            }
        }
    }

    char get(int x, int y) {
        if (y == M) {
            y = 0;
        }
        if (y == -1) {
            y = M    - 1;
        }
        if (x == -1) {
            return borders[0][y];
        }
        if (x == N) {
            return borders[1][y];
        }
        return field[curField][x][y];
    }

    bool isAlive(int x, int y) {
        return get(x, y) == '*';
    }

    bool makeMove(int x, int y) {
        int neighboursCount = 0;
        for (int i = 0; i < MOVE_CNT; ++i) {
            neighboursCount += isAlive(x + moves[i].dx, y + moves[i].dy);
        }
        if (!isAlive(x, y)) {
            return (neighboursCount == 3);
        } else {
            return (2 <= neighboursCount && neighboursCount <= 3);
        }
    }


    void startCalculation() {
        connectToNeighbours();
        me->initSockets();
        initMoves();

        curField = 0;
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < M; ++j) {
                printf("%c", field[curField][i][j]);
            }
            printf("\n");
        }

        for (int i = 0; i < 2; ++i) {
            borders[i] = new char[M];
        }


        for (int k = 0; k < steps; ++k) {
            writeSomething(neighbours[0]->sd, &index, sizeof(int));
            writeSomething(neighbours[0]->sd, field[curField][0], M);
            writeSomething(neighbours[1]->sd, &index, sizeof(int));
            writeSomething(neighbours[1]->sd, field[curField][N - 1], M);
            // printf("Trying to read\n");
            me->receiveData(borders, M, (index + 1) % total);
            // printf("Read\n");
            for (int i = 0; i < N; ++i) {
                for (int j = 0; j < M; ++j) {
                    field[1 ^ curField][i][j] = STATE[makeMove(i, j)];
                }
            }
            curField ^= 1;
            // closeNeighbours();
        }
        printf("-----------------------\n");
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < M; ++j) {
                printf("%c", field[curField ^ 1][i][j]);
            }
            printf("\n");
        }
        printf("-----------------------\n");
    }

    ~Node() {
        for (int j = 0; j < 2; ++j) {
            for (int i = 0; i < N; ++i) {
                delete[] field[j][i];
            }
            delete[] borders[j];
        }
        delete server;
        delete neighbours[0];
        delete neighbours[1];
    }
};

const char Node::STATE[2] = {'.', '*'};

#endif
