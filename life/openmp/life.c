#include <stdio.h>
#include <omp.h>
#include <stdlib.h>

#define MAXN 1000

int field[2][MAXN][MAXN];
int readField = 0;

int n, m;

struct move {
    int dx;
    int dy;
};

struct move make_pair(int first, int second) {
    struct move result;
    result.dx = first;
    result.dy = second;
    return result;
}

#define DIRS_COUNT 8
struct move dirs[DIRS_COUNT];

void init() {
    int cnt = 0;
    for (int dx = -1; dx <= 1; ++dx) {
        for (int dy = -1; dy <= 1; ++dy) {
            if (dx || dy) {
                dirs[cnt++] = make_pair(dx, dy);
            }
        }
    }
}

int inc(int x, int dx, int n) {
    x += dx;
    if (x == n)
        x = 0;
    if (x < 0)
        x = n - 1;
    return x;
}

int incX(int x, int dx) {
    return inc(x, dx, n);
}

int incY(int y, int dy) {
    return inc(y, dy, m);
}

int isAlive(int x, int y) {
    return field[readField][x][y];
}

void make_move(int x, int y) {
    int neighboursCount = 0;
    for (int i = 0; i < DIRS_COUNT; ++i) {
        neighboursCount += isAlive(incX(x, dirs[i].dx), incY(y, dirs[i].dy));
    }
    if (!isAlive(x, y)) {
        field[1 ^ readField][x][y] = (neighboursCount == 3);
    } else {
        field[1 ^ readField][x][y] = (2 <= neighboursCount &&
                                      neighboursCount <= 3);
    }
}

char chars[2] = {'.', '*'};

int steps;

void changeRegion(int start, int end) {
    for (int i = 0; i < steps; ++i) {
        for (int x = start; x < end; ++x) {
            for (int y = 0; y < m; ++y) {
                make_move(x, y);
            }
        }
        #pragma omp barrier
        #pragma omp single
        {
            readField ^= 1;
        }

    }
}

int min(int a, int b) {
    return (a < b ? a : b);
}

int main(int argc, char *argv[])
{
    init();
    int p = atoi(argv[1]);
    steps = atoi(argv[2]);
    FILE *in = fopen("../input.txt", "r");
    fscanf(in, "%d %d", &n, &m);
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            char c;
            fscanf(in, " %c", &c);
            field[0][i][j] = (c == '*');
            printf("%c", chars[field[0][i][j]]);
        }
        printf("\n");
    }
    p = min(p, n);
    int *args = malloc(2 * p * sizeof(int));
    int start = 0;
    int end = 0;
    for (int i = 0; i < p; ++i) {
        start = end;
        end = start + (n / p) + (i < (n % p));
        args[2 * i] = start;
        args[2 * i + 1] = end;
    }

    #pragma omp parallel num_threads (p)
    {
        int i = omp_get_thread_num();
        printf("Starting %d: %d %d\n", i, args[2 * i], args[2 * i + 1]);
        changeRegion(args[2 * i], args[2 * i + 1]);
    }

    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            printf("%c", chars[field[readField][i][j]]);
        }
        printf("\n");
    }
}
