#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

int **allocarray(int n, int m) {
    int *data = (int *) malloc(n * m * sizeof(int));
    int **arr = (int **) malloc(n * sizeof(int *));
    for (int i = 0; i < n; i++)
        arr[i] = &(data[i * m]);
    return arr;
}

struct Node {
    int world_size;
    int world_rank;
    int steps;
    int m, n;
    int N;

    int *args;
    int **field;
    int *allField;
    int *displs;
    int *sendCount;
    MPI_Status status;

    int *neighbours[2];
    int prev;
    int next;

    struct Move {
        int dx;
        int dy;
    };

    static const int MOVE_CNT = 8;
    Move moves[MOVE_CNT];
    int **myField[2];
    int curField;

    void init_root(int argc, char **argv) {
        int n, m;

        FILE *fieldFile = fopen(argv[1], "r");
        fscanf(fieldFile, "%d %d", &n, &m);
        N = n;
        field = allocarray(N, m);
        allField = &(field[0][0]);

        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                char c;
                fscanf(fieldFile, " %c", &c);
                field[i][j] = (c == '*');
            }
        }

        args = (int *) malloc(4 * world_size * sizeof(int));
        displs = (int *) malloc(world_size * sizeof(int));
        sendCount = (int *) malloc(world_size * sizeof(int));
        int start = 0;
        int end = 0;
        for (int i = 0; i < world_size; ++i) {
            start = end;
            end = start + (n / world_size) + (i < (n % world_size));
            args[4 * i] = start;
            displs[i] = start * m;
            args[4 * i + 1] = end;
            sendCount[i] = (end - start) * m;
            args[4 * i + 2] = m;
            args[4 * i + 3] = n;
        }
        for (int i = 0; i < world_size; ++i) {
            printf("%d %d\n", displs[i], sendCount[i]);
        }
    }

    void initMoves() {
        int cnt = 0;
        for (int dx = -1; dx <= 1; ++dx) {
            for (int dy = -1; dy <= 1; ++dy) {
                if (dx || dy) {
                    moves[cnt++] = {dx, dy};
                }
            }
        }
    }

    char get(int x, int y) {
        // printf("GET %d %d %d\n", x, y, world_rank);
        if (y == m) {
            y = 0;
        }
        if (y == -1) {
            y = m - 1;
        }
        if (x == -1) {
            return neighbours[0][y];
        }
        if (x == n) {
            return neighbours[1][y];
        }
        // printf("GETGET %d %d %d\n", curField, x, y);
        return myField[curField][x][y];
    }

    bool isAlive(int x, int y) {
        return get(x, y);
    }

    bool makeMove(int x, int y) {
        // printf("MAKE MOVE %d %d %d\n", x, y, world_rank);
        int neighboursCount = 0;
        for (int i = 0; i < MOVE_CNT; ++i) {
            neighboursCount += isAlive(x + moves[i].dx, y + moves[i].dy);
        }
        if (!isAlive(x, y)) {
            return (neighboursCount == 3);
        } else {
            return (2 <= neighboursCount && neighboursCount <= 3);
        }
    }

    void initCalc() {
        printf("HERE %d\n", world_rank);
        MPI_Send(myField[0][0], m,  MPI_INT, prev, 0, MPI_COMM_WORLD);
        MPI_Send(myField[0][n - 1], m, MPI_INT, next, 0, MPI_COMM_WORLD);

        MPI_Recv(neighbours[0], m, MPI_INT, prev, 0,
                 MPI_COMM_WORLD, &status);
        MPI_Recv(neighbours[1], m, MPI_INT, next, 0,
            MPI_COMM_WORLD, &status);
        printf("THERE %d\n", world_rank);
    }


    void calculate() {
        printf("CALC %d\n", world_rank);
        for (int k = 0;  k < steps; ++k) {
            for (int j = 0; j < m; ++j) {
                // printf("%d %d %d\n", k, j, world_rank);
                myField[1 ^ curField][0][j] = makeMove(0, j);
                myField[1 ^ curField][n - 1][j] = makeMove(n - 1, j);
            }

            MPI_Send(myField[1 ^ curField][0], m,  MPI_INT, prev, 0, MPI_COMM_WORLD);
            MPI_Send(myField[1 ^ curField][n - 1], m, MPI_INT, next, 0, MPI_COMM_WORLD);

            for (int i = 1; i < n - 1; ++i) {
                for (int j = 0; j < m; ++j) {
                    myField[1 ^ curField][i][j] = makeMove(i, j);
                }
            }
            MPI_Recv(neighbours[0], m, MPI_INT, prev, 0,
                     MPI_COMM_WORLD, &status);
            MPI_Recv(neighbours[1], m, MPI_INT, next, 0,
                MPI_COMM_WORLD, &status);
            curField ^= 1;
        }
    }

    void finish() {
        printf("FINISH %d\n", world_rank);
        if (world_rank == 0) {
            for (int i = 0; i < world_size; ++i) {
                printf("%d %d\n", sendCount[i], displs[i]);
            }
        }

        printf("%d %d\n", n * m, world_rank);

        MPI_Gatherv(myField[curField][0], n * m, MPI_INT, allField,
                    sendCount, displs, MPI_INT, 0, MPI_COMM_WORLD);

        if (world_rank == 0) {
            printf("END!");
            finishRoot();
        }
    }

    void finishRoot() {
        printf("-----------\n");
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < m; ++j) {
                printf("%c", field[i][j] ? '*' : '.');
            }
            printf("\n");
        }
    }

    Node(int argc, char **argv) {
        MPI_Init(&argc, &argv);
        printf("%d %s %s\n", argc, argv[1], argv[2]);
        MPI_Comm_size(MPI_COMM_WORLD, &world_size);
        MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

        args = NULL;
        allField = NULL;
        field = NULL;
        displs = NULL;
        sendCount = NULL;
        steps = atoi(argv[2]);

        if (world_rank == 0) {
            printf("MASTER\n");
            init_root(argc, argv);
        }


        int *myStartEnd = (int *) malloc(sizeof(int) * 4);

        MPI_Scatter(args, 4, MPI_INT, myStartEnd, 4, MPI_INT, 0, MPI_COMM_WORLD);

        m = myStartEnd[2];
        n = myStartEnd[1] - myStartEnd[0];

        curField = 0;
        printf("alloc %d %d %d\n", n, m, world_rank);
        myField[0] = allocarray(n, m);
        myField[1] = allocarray(n, m);

        neighbours[0] = (int *) malloc(sizeof(int) * m);
        neighbours[1] = (int *) malloc(sizeof(int) * m);
        prev = (world_rank - 1 + world_size) % world_size;
        next = (world_rank + 1) % world_size;
        initMoves();
        initCalc();
        MPI_Scatterv(allField, sendCount, displs, MPI_INT,
                     &(myField[curField][0][0]),
                     n * m, MPI_INT, 0, MPI_COMM_WORLD);

        if (world_rank == 1) {
            for (int  i = 0; i < n; ++i) {
                for (int j = 0; j < m; ++j) {
                    printf("%c", myField[curField][i][j] ? '*' : '.');
                }
                printf("\n");
            }
        }
    }

    ~Node() {
        MPI_Finalize();
        free(allField);
        free(field);
        free(args);
        free(displs);
        free(sendCount);
        free(myField[0]);
        free(myField[1]);
        free(neighbours[0]);
        free(neighbours[1]);
    }
};

int main(int argc, char** argv) {
    Node node(argc, argv);
    node.calculate();
    node.finish();
}
