#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>


#define MAXN 1000

int field[2][MAXN][MAXN];
int state[MAXN][MAXN];
int err = 0;

int n, m;

struct move {
    int dx;
    int dy;
};

struct move make_pair(int first, int second) {
    struct move result;
    result.dx = first;
    result.dy = second;
    return result;
}

#define DIRS_COUNT 8
struct move dirs[DIRS_COUNT];

void init() {
    int cnt = 0;
    for (int dx = -1; dx <= 1; ++dx) {
        for (int dy = -1; dy <= 1; ++dy) {
            if (dx || dy) {
                dirs[cnt++] = make_pair(dx, dy);
            }
        }
    }
}

int inc(int x, int dx, int n) {
    x += dx;
    if (x == n)
        x = 0;
    if (x < 0)
        x = n - 1;
    return x;
}

int incX(int x, int dx) {
    return inc(x, dx, n);
}

int incY(int y, int dy) {
    return inc(y, dy, m);
}

int isAlive(int x, int y, int step) {
    return field[step & 1][x][y];
}

void make_move(int x, int y, int step) {
    int prev_step = step - 1;
    int neighboursCount = 0;
    for (int i = 0; i < DIRS_COUNT; ++i) {
        int newX = incX(x, dirs[i].dx);
        int newY = incY(y, dirs[i].dy);
        while (state[newX][newY] < prev_step){
            sched_yield();
        }
        neighboursCount += isAlive(newX, newY, prev_step);
    }
    if (!isAlive(x, y, prev_step)) {
        field[(step & 1)][x][y] = (neighboursCount == 3);
    } else {
        field[(step & 1)][x][y] = (2 <= neighboursCount &&
                                      neighboursCount <= 3);
    }
    state[x][y] += 1;
}

char chars[2] = {'.', '*'};

int p;

int steps;

void processLine(int x, int step) {
    for (int y = 0; y < m; ++y) {
        make_move(x, y, step + 1);
    }
}

void *changeRegion(void *args) {
    int start = ((int *)args)[0];
    int end = ((int *)args)[1];
    for (int i = 0; i < steps; ++i) {
        processLine(start, i);
        if (start != end - 1) {
            processLine(end - 1, i);
        }
        for (int x = start + 1; x < end - 1; ++x) {
            processLine(x, i);
        }
    }

}

int min(int a, int b) {
    return (a < b ? a : b);
}

int main(int argc, char **argv) {
    init();
    p = atoi(argv[1]);
    steps = atoi(argv[2]);
    FILE *in = fopen("../input.txt", "r");
    fscanf(in, "%d %d", &n, &m);
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            char c;
            fscanf(in, " %c", &c);
            field[0][i][j] = (c == '*');
            printf("%c", chars[field[0][i][j]]);
        }
        printf("\n");
    }
    printf("--------------------\n");
    p = min(p, n);
    pthread_t *threads = (pthread_t *) malloc(p * sizeof(pthread_t));
    int *args = malloc(2 * p * sizeof(int));
    int start = 0;
    int end = 0;

    for (int i = 0; i < p; ++i) {
        start = end;
        end = start + (n / p) + (i < (n % p));
        args[2 * i] = start;
        args[2 * i + 1] = end;
        pthread_create(&threads[i], NULL, changeRegion, &args[2 * i]);
    }

    for (int i = 0; i < p; ++i) {
        void *result;
        pthread_join(threads[i], &result);
    }
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            printf("%c", chars[field[steps & 1][i][j]]);
        }
        printf("\n");
    }
    printf("%d\n", err);
}
